package com.muhardin.endy.postilion.client;

import org.jpos.iso.ISOMsg;
import org.jpos.iso.channel.PostChannel;
import org.jpos.iso.packager.PostPackager;

public class EchoClient {
    public static void main(String[] args) throws Exception {
        System.out.println("Test Send Echo to Postilion");

        String postilionServer = "127.0.0.1";
        Integer postilionPort = 12345;

        ISOMsg echoRequest = new ISOMsg();
        echoRequest.setMTI("0800");
        echoRequest.set(7,"0122101010");
        echoRequest.set(11,"112233");
        echoRequest.set(12,"101010");
        echoRequest.set(13,"0122");
        echoRequest.set(70,"301");

        PostPackager postPackager = new PostPackager();
        echoRequest.setPackager(postPackager);
        String strEchoRequest = new String(echoRequest.pack());
        System.out.println("Echo Request : "+strEchoRequest);

        PostChannel postChannel = new PostChannel(postilionServer, postilionPort, postPackager);
        postChannel.connect();

        postChannel.send(echoRequest);
        ISOMsg echoResponse = postChannel.receive();

        String strEchoResponse = new String(echoResponse.pack());
        System.out.println("Echo Response : "+strEchoResponse);
    }
}
